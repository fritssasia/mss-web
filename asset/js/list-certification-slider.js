// List Certification Slider
new Splide("#splide-list-certification-slider", {
    perPage: 3,
    perMove: 1,
    rewind: true,
    autoplay: true,
    pauseOnHover: false,
    pauseOnFocus: false,
    rewindSpeed: 1000,
    speed: 1000,
    easing: 'ease-out',
    pagination: false,
    gap: '1rem',
    breakpoints: {
        500: {
            perPage: 1,
        },
    },
}).mount();