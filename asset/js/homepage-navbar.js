// Change navbar background color on scroll
$(function () {
    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 10) {
            $('.navbar').addClass('navbar-active');
        } else {
            $('.navbar').removeClass('navbar-active');
        }
    });
});