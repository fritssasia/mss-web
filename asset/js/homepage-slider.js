// Homepage Slider
new Splide("#splide-homepage-slider", {
    perPage: 1,
    perMove: 1,
    rewind: true,
    pauseOnHover: false,
    pauseOnFocus: false,
    rewindSpeed: 1000,
    speed: 1000,
    easing: 'ease-out',
    pagination: false,
    gap: '1rem',
}).mount();